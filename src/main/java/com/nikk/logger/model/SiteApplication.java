package com.nikk.logger.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "applications", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "name"
        }),
        @UniqueConstraint(columnNames = {
                "code"
        })
})
public class SiteApplication {
  @Id
    @ApiModelProperty(notes = "The database generated application ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @ApiModelProperty(notes = "The date of creation of the log message")
    @Size(min=3, max = 50)
    private String name;

    @NotBlank
    @ApiModelProperty(notes = "The unique application code")
    @Size(min=3, max = 30)
    private String code;

    @Column(name = "created_by")
    @ApiModelProperty(notes = "The username of the application entry creator")
    private String createdBy;

    @NotBlank
    @ApiModelProperty(notes = "The pattern used for generating log file names e.g. YYYY_MM_DD")
    @Column(name = "log_pattern")
    private String logPattern;

    @ApiModelProperty(notes = "The application creation date")
    @Column(name = "created_at")
    private Date createdAt;


    public SiteApplication() {}

    public SiteApplication(int id) {
        this.id = id;
    }

    public SiteApplication(String name, String code, String createdBy, Date createdAt, String logPattern) {
        this.name = name;
        this.code = code;
        this.logPattern = logPattern;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
    }


    public SiteApplication(int id, String name, String code, String createdBy, Date createdAt,
                           String logPattern, Set<ApplicationLog> applicationLogs,
                           Set<UserAppPermission> userAppPermissions) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.logPattern = logPattern;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.applicationLogs = applicationLogs;
        this.userAppPermissions = userAppPermissions;
    }

    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getLogPattern() {
        return logPattern;
    }

    public void setLogPattern(String logPattern) {
        this.logPattern = logPattern;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "siteApplication")
    private Set<ApplicationLog> applicationLogs = new HashSet<ApplicationLog>(0);
    public Set<ApplicationLog> getApplicationLogs() {
        return applicationLogs;
    }
    public void setApplicationLogs(Set<ApplicationLog> applicationLogs) {
        this.applicationLogs = applicationLogs;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "siteApplication")
    private Set<UserAppPermission> userAppPermissions = new HashSet<UserAppPermission>(0);
    public Set<UserAppPermission> getUserAppPermissions() {
        return userAppPermissions;
    }
    public void setUserAppPermissions(Set<UserAppPermission> userAppPermissions) {
        this.userAppPermissions = userAppPermissions;
    }

}
