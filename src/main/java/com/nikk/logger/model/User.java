package com.nikk.logger.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NaturalId;
 
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
            "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
public class User{
  @Id
    @ApiModelProperty(notes = "The database generated user ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
 
    @NotBlank
    @ApiModelProperty(notes = "User's full names")
    @Size(min=3, max = 50)
    private String name;
 
    @NaturalId
    @ApiModelProperty(notes = "User's unique username")
    @NotBlank
    @Size(max = 30)
    private String username;

    @NaturalId
    @ApiModelProperty(notes = "User's email address")
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
 
    @NotBlank
    @ApiModelProperty(notes = "The user password")
    @Size(min=6, max = 100)
    private String password;

 
    public User() {}
 
    public User(String name, String username, String email, String password) {
        this.name = name;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public User(String name, String username, String email, String password
            ,Set<UserAppPermission> userAppPermissions) {
        this.name = name;
        this.email = email;
        this.username = username;
        this.password = password;
        this.userAppPermissions = userAppPermissions;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }


    public String getUsername() {
        return name;
    }

    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserAppPermission> userAppPermissions = new HashSet<UserAppPermission>(0);

    public Set<UserAppPermission> getUserAppPermissions() {
        return userAppPermissions;
    }
    public void setUserAppPermissions(Set<UserAppPermission> userAppPermissions) {
        this.userAppPermissions = userAppPermissions;
    }

}
