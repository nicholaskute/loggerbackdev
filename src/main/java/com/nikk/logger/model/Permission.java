package com.nikk.logger.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "permissions", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
            "id"
        }),
        @UniqueConstraint(columnNames = {
                "name"
        })
})
public class Permission {
  @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @Size(min=3, max = 30)
    private String name;


    public Permission() {}

    public Permission(int id) {
        this.id = id;
    }

    public Permission(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Permission(int id, String name, Set<UserAppPermission> userAppPermissions) {
        this.id = id;
        this.name = name;
        this.userAppPermissions = userAppPermissions;
    }

    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "permission")
    private Set<UserAppPermission> userAppPermissions = new HashSet<UserAppPermission>(0);

    public Set<UserAppPermission> getUserAppPermissions() {
        return userAppPermissions;
    }
    public void setUserAppPermissions(Set<UserAppPermission> userAppPermissions) {
        this.userAppPermissions = userAppPermissions;
    }

}
