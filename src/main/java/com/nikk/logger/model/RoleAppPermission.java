package com.nikk.logger.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "role_app_permissions", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
            "app_id"
        }),
        @UniqueConstraint(columnNames = {
                "role_id"
        }),
        @UniqueConstraint(columnNames = {
                "permission_id"
        })
})
public class RoleAppPermission {
  @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @Column(name = "app_id")
    private int app_id;

    @NotBlank
    @Column(name = "role_id")
    private int role_id;

    @NotBlank
    @Column(name = "permission_id")
    private int permission_id;

    @NotBlank
    @Column(name = "created_at")
    private Date createdAt;

    @NotBlank
    @Column(name = "created_by")
    private String createdBy;


    public RoleAppPermission() {}


    public RoleAppPermission(int app_id, int role_id, int permission_id) {
        this.app_id = app_id;
        this.role_id = role_id;
        this.permission_id = permission_id;
    }

    public RoleAppPermission(int app_id, int role_id, int permission_id, Date createdAt,
                             String createdBy, SiteApplication siteApplication,
                             User user, Permission permission) {
        this.app_id = app_id;
        this.role_id = role_id;
        this.permission_id = permission_id;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.siteApplication = siteApplication;
        this.user = user;
        this.permission = permission;
    }

    //siteapplication - many to one relation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_id", insertable = false, updatable = false)
    private SiteApplication siteApplication;

    public SiteApplication getApplication() {
        return siteApplication;
    }
    public void setApplication(SiteApplication siteApplication) {
        this.siteApplication = siteApplication;
    }

    //users - many to one relation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    //permissions - many to one relation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "permission_id", insertable = false, updatable = false)
    private Permission permission;

    public Permission getPermission() {
        return permission;
    }
    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public int getPermission_id() {
        return permission_id;
    }

    public void setPermission_id(int permission_id) {
        this.permission_id = permission_id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
