package com.nikk.logger.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "application_logs", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
            "id"
        })
})
public class ApplicationLog {
  @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //@NotBlank
    @ApiModelProperty(notes = "The database generated application log ID")
    @Column(name = "app_id")
    private int app_id;

    @NotBlank
    @ApiModelProperty(notes = "The log message text")
    private String message;

    @NotBlank
    @ApiModelProperty(notes = "The log level of message e.g. WARN, INFO, ERROR, CRITICAL")
    @Column(name = "log_level")
    private String log_level;

    //@NotBlank
    @ApiModelProperty(notes = "The log message creation date")
    @Column(name = "created_at")
    private Date created_at;


    public ApplicationLog() {}

    public ApplicationLog(Long id) {
        this.id = id;
    }

    public ApplicationLog(String message, String log_level,
                          Date created_at, SiteApplication siteApplication) {
        this.message = message;
        this.log_level = log_level;
        this.created_at = created_at;
        this.siteApplication = siteApplication;
    }

    public ApplicationLog(Long id, int app_id, String message, String log_level,
                          Date created_at, SiteApplication siteApplication) {
        this.id = id;
        this.app_id = app_id;
        this.message = message;
        this.log_level = log_level;
        this.created_at = created_at;
        this.siteApplication = siteApplication;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLog_level() {
        return log_level;
    }

    public void setLog_level(String log_level) {
        this.log_level = log_level;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }


    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_id", insertable = false, updatable = false)
    private SiteApplication siteApplication;

    public SiteApplication getSiteApplication() {
        return this.siteApplication;
    }

    public void setSiteApplication(SiteApplication siteApplication) {
        this.siteApplication = siteApplication;
    }


}
