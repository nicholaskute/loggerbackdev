package com.nikk.logger.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "user_app_permissions", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
            "app_id"
        }),
        @UniqueConstraint(columnNames = {
                "user_id"
        }),
        @UniqueConstraint(columnNames = {
                "permission_id"
        })
})
public class UserAppPermission {
  @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @Column(name = "app_id")
    private int appId;

    @NotBlank
    @Column(name = "user_id")
    private int userId;

    @NotBlank
    @Column(name = "permission_id")
    private int permissionId;

    @NotBlank
    @Column(name = "created_at")
    private Date createdAt;

    @NotBlank
    @Column(name = "created_by")
    private String createdBy;


    public UserAppPermission() {}


    public UserAppPermission(int appId, int userId, int permissionId) {
        this.appId = appId;
        this.userId = userId;
        this.permissionId = permissionId;
    }

    public UserAppPermission(int appId, int userId, int permissionId, Date createdAt,
                             String createdBy, SiteApplication siteApplication,
                             User user, Permission permission) {
        this.appId = appId;
        this.userId = userId;
        this.permissionId = permissionId;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.siteApplication = siteApplication;
        this.user = user;
        this.permission = permission;
    }

    //siteapplication - many to one relation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_id", insertable = false, updatable = false)
    private SiteApplication siteApplication;

    public SiteApplication getApplication() {
        return siteApplication;
    }
    public void setApplication(SiteApplication siteApplication) {
        this.siteApplication = siteApplication;
    }

    //users - many to one relation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    //permissions - many to one relation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "permission_id", insertable = false, updatable = false)
    private Permission permission;

    public Permission getPermission() {
        return permission;
    }
    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public int getApp_id() {
        return appId;
    }

    public void setApp_id(int appId) {
        this.appId = appId;
    }

    public int getUser_id() {
        return userId;
    }

    public void setUser_id(int userId) {
        this.userId = userId;
    }

    public int getPermission_id() {
        return permissionId;
    }

    public void setPermission_id(int permissionId) {
        this.permissionId = permissionId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
