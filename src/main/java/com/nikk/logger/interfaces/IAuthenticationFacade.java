package com.nikk.logger.interfaces;

import org.springframework.security.core.Authentication;

/**
 * Created by nikk on 4/16/2019.
 */
public interface IAuthenticationFacade {
    Authentication getAuthentication();
}
