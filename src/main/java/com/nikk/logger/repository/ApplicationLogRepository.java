package com.nikk.logger.repository;

import com.nikk.logger.model.ApplicationLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationLogRepository extends JpaRepository<ApplicationLog, Long>{
    
}
