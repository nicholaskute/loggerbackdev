package com.nikk.logger.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nikk.logger.model.Role;
import com.nikk.logger.model.RoleName;

public interface RoleRepository extends JpaRepository<Role, Long>{
	Optional<Role> findByName(RoleName roleName);
}
