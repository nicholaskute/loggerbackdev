package com.nikk.logger.repository;

import com.nikk.logger.model.UserAppPermission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAppPermissionRepository extends JpaRepository<UserAppPermission, Long>{
    
}
