package com.nikk.logger.repository;

import com.nikk.logger.model.SiteApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ApplicationRepository extends JpaRepository<SiteApplication, Integer>{

    Optional<SiteApplication> findByName(String username);
    Boolean existsByName(String name);
    Boolean existsByCode(String code);
    
}
