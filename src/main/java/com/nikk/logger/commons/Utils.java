package com.nikk.logger.commons;

import com.nikk.logger.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class Utils {

    private static final Utils instance = new Utils();

    private static Utils getInstance(){
        return instance;
    }

    public Utils(){}

    public String getLoggedInUsername() {

        //get currently logged in user
        String currentUserName = "";

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            currentUserName = ((UserDetails)principal).getUsername();
        } else {
            currentUserName = principal.toString();
        }

        return currentUserName;

    }

}
