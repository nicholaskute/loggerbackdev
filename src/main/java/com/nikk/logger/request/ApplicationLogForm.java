package com.nikk.logger.request;

import javax.validation.constraints.NotBlank;

public class ApplicationLogForm {

    //@NotBlank
    private Long id;

    private int appId;
 
    @NotBlank
    private String logLevel;

    @NotBlank
    private String message;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
