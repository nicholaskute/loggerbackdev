package com.nikk.logger.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ApplicationForm {


    @NotBlank
    @Size(min=3, max = 50)
    private String name;
 
    @NotBlank
    @Size(min = 3, max = 30)
    private String code;


    @NotBlank
    private String log_pattern;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLog_pattern() {
        return log_pattern;
    }

    public void setLog_pattern(String log_pattern) {
        this.log_pattern = log_pattern;
    }


}
