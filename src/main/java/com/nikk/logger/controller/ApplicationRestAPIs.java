package com.nikk.logger.controller;

import com.nikk.logger.exception.ResourceNotFoundException;
import com.nikk.logger.interfaces.IAuthenticationFacade;
import com.nikk.logger.model.SiteApplication;
import com.nikk.logger.repository.ApplicationRepository;
import com.nikk.logger.repository.RoleRepository;
import com.nikk.logger.request.ApplicationForm;
import com.nikk.logger.security.jwt.JwtProvider;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/applications")
public class ApplicationRestAPIs {
 
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    IAuthenticationFacade authenticationFacade;
 
    @Autowired
    ApplicationRepository applicationRepository;
 
    @Autowired
    RoleRepository roleRepository;
 
    @Autowired
    PasswordEncoder encoder;
 
    @Autowired
    JwtProvider jwtProvider;


    @ApiOperation(value = "View a list of available applications", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("")
    List<SiteApplication> all() {
        return applicationRepository.findAll();
    }


    @ApiOperation(value = "Get an application by Id")
    @GetMapping("/{id}")
    public ResponseEntity < SiteApplication > getUserById(
            @ApiParam(value = "Application id from which application object will be retrieved", required = true) @PathVariable(value = "id") int applicationId)
            throws ResourceNotFoundException {
        SiteApplication application = applicationRepository.findById(applicationId)
                .orElseThrow(()-> new ResourceNotFoundException("Application not found for this id :: " + applicationId));
        return ResponseEntity.ok().body(application);
    }


    @ApiOperation(value = "Add an application")
    @PostMapping("")
    public ResponseEntity<String> createApplication(@Valid @RequestBody ApplicationForm applicationForm) {

        if(applicationRepository.existsByName(applicationForm.getName())) {
            return new ResponseEntity<String>("Fail -> Application name is already in use!",
                    HttpStatus.BAD_REQUEST);
        }

        if(applicationRepository.existsByCode(applicationForm.getCode())) {
            return new ResponseEntity<String>("Fail -> Application code is already in use!",
                    HttpStatus.BAD_REQUEST);
        }

        //get currently logged in user
        String currentUserName = "";
        try {

            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof UserDetails) {
                currentUserName = ((UserDetails)principal).getUsername();
            } else {
                currentUserName = principal.toString();
            }


        } catch (Exception e) {
            return new ResponseEntity<String>("An error occured: " + e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating new siteApplication
        SiteApplication siteApplication = new SiteApplication(applicationForm.getName(), applicationForm.getCode(),
                currentUserName, new Date(), applicationForm.getLog_pattern());
        
        applicationRepository.save(siteApplication);
 
        return new ResponseEntity<String>("Application created successfully!",
                HttpStatus.OK);
    }



    @ApiOperation(value = "Update an application")
    @PutMapping("/{id}")
    public ResponseEntity <SiteApplication> updateApplication(
            @ApiParam(value = "Application Id to update application object", required = true) @PathVariable(value = "id") int applicationId,
            @ApiParam(value = "Update application object", required = true) @Valid @RequestBody SiteApplication applicationDetails) throws ResourceNotFoundException {
        SiteApplication application = applicationRepository.findById(applicationId)
                .orElseThrow(()-> new ResourceNotFoundException("Application not found for this id :: " + applicationId));

        application.setName(applicationDetails.getName());
        application.setCode(applicationDetails.getCode());
        application.setLogPattern(applicationDetails.getLogPattern());
        application.setCreatedAt(new Date());

        final SiteApplication updatedApplication = applicationRepository.save(application);
        return ResponseEntity.ok(updatedApplication);
    }


    @ApiOperation(value = "Delete an application")
    @DeleteMapping("/{id}")
    public Map< String, Boolean > deleteApplication(
            @ApiParam(value = "Application Id from which application object will be deleted", required = true) @PathVariable(value = "id") int applicationId)
            throws ResourceNotFoundException {
        SiteApplication application = applicationRepository.findById(applicationId)
                .orElseThrow(() -> new ResourceNotFoundException("Application not found for this id :: " + applicationId));
        applicationRepository.delete(application);
        Map < String, Boolean > response = new HashMap< >();
        response.put("deleted", Boolean.TRUE);
        return response;
    }


}