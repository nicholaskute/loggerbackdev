package com.nikk.logger.controller;

import com.nikk.logger.exception.ResourceNotFoundException;
import com.nikk.logger.interfaces.IAuthenticationFacade;
import com.nikk.logger.model.ApplicationLog;
import com.nikk.logger.model.SiteApplication;
import com.nikk.logger.repository.ApplicationLogRepository;
import com.nikk.logger.request.ApplicationLogForm;
import com.nikk.logger.security.jwt.JwtProvider;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/application-logs")
public class ApplicationLogRestAPIs {
 
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private static IAuthenticationFacade authenticationFacade;
 
    @Autowired
    ApplicationLogRepository applicationLogRepository;
 
    @Autowired
    JwtProvider jwtProvider;


    @ApiOperation(value = "View a list of available application logs", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("")
    List<ApplicationLog> all() {
        return applicationLogRepository.findAll();
    }


    @ApiOperation(value = "Get an application log by Id")
    @GetMapping("/{id}")
    public ResponseEntity < ApplicationLog > getUserById(
            @ApiParam(value = "Application Log id from which application log object will be retrieved", required = true) @PathVariable(value = "id") Long applicationLogId)
            throws ResourceNotFoundException {
        ApplicationLog applicationLog = applicationLogRepository.findById(applicationLogId)
                .orElseThrow(()-> new ResourceNotFoundException("ApplicationLog not found for this id :: " + applicationLogId));
        return ResponseEntity.ok().body(applicationLog);
    }


    @ApiOperation(value = "Add an application log")
    @PostMapping("")
    public ResponseEntity<String> createApplicationLog(@Valid @RequestBody ApplicationLogForm applicationForm) {

        // Creating new applicationLog
        ApplicationLog applicationLog = new ApplicationLog(applicationForm.getMessage(),
                applicationForm.getLogLevel(),
                new Date(), new SiteApplication(applicationForm.getAppId()));
        
        applicationLogRepository.save(applicationLog);
 
        return new ResponseEntity<String>("Application Log created successfully!",
                HttpStatus.OK);

    }



    /*@ApiOperation(value = "Update an application log")
    @PutMapping("/{id}")
    public ResponseEntity <ApplicationLog> updateApplicationLog(
            @ApiParam(value = "Application Log Id to update application log object", required = true) @PathVariable(value = "id") Long applicationLogId,
            @ApiParam(value = "Update application log object", required = true) @Valid @RequestBody ApplicationLog applicationLogDetails) throws ResourceNotFoundException {
        ApplicationLog applicationLog = applicationLogRepository.findById(applicationLogId)
                .orElseThrow(()-> new ResourceNotFoundException("Application Log not found for this id :: " + applicationLogId));

        applicationLog.setMessage(applicationLogDetails.getMessage());
        applicationLog.setLog_level(applicationLogDetails.getLog_level());
        applicationLog.setSiteApplication(new SiteApplication(applicationLogDetails.getApp_id()));

        final ApplicationLog updatedApplicationLog = applicationLogRepository.save(applicationLog);
        return ResponseEntity.ok(updatedApplicationLog);
    }
*/


    @ApiOperation(value = "Delete an application log")
    @DeleteMapping("/{id}")
    public Map< String, Boolean > deleteApplicationLog(
            @ApiParam(value = "ApplicationLog Id from which application log object will be deleted", required = true) @PathVariable(value = "id") Long applicationLogId)
            throws ResourceNotFoundException {
        ApplicationLog applicationLog = applicationLogRepository.findById(applicationLogId)
                .orElseThrow(() -> new ResourceNotFoundException("Application Log not found for this id :: " + applicationLogId));
        applicationLogRepository.delete(applicationLog);
        Map < String, Boolean > response = new HashMap< >();
        response.put("deleted", Boolean.TRUE);
        return response;
    }


}