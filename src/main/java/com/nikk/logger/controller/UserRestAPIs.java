package com.nikk.logger.controller;

import com.nikk.logger.exception.ResourceNotFoundException;
import com.nikk.logger.interfaces.IAuthenticationFacade;
import com.nikk.logger.model.User;
import com.nikk.logger.repository.RoleRepository;
import com.nikk.logger.repository.UserRepository;
import com.nikk.logger.request.SignUpForm;
import com.nikk.logger.security.jwt.JwtProvider;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value="User Management", description="Operations pertaining to users in Log  Management System")

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/users")
public class UserRestAPIs {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;


    @ApiOperation(value = "View a list of available users", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("")
    List<User> all() {
        return userRepository.findAll();
    }

    @ApiOperation(value = "Get a user by Id")
    @GetMapping("/users/{id}")
    public ResponseEntity < User > getUserById(
            @ApiParam(value = "User id from which user object will be retrieved", required = true) @PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("User not found for this id :: " + userId));
        return ResponseEntity.ok().body(user);
    }

    @ApiOperation(value = "Get logged on user")
    @GetMapping("/currentuser")
    public Authentication currentUserData() {
        Authentication authentication = authenticationFacade.getAuthentication();
        return authentication;
    }

    @ApiOperation(value = "Add a user")
    @PostMapping("")
    public ResponseEntity<String> createUser(@Valid @RequestBody SignUpForm signupForm) {

        if(userRepository.existsByUsername(signupForm.getName())) {
            return new ResponseEntity<String>("Fail -> Username is already in use!",
                    HttpStatus.BAD_REQUEST);
        }

        if(userRepository.existsByEmail(signupForm.getEmail())) {
            return new ResponseEntity<String>("Fail -> Email address is already in use!",
                    HttpStatus.BAD_REQUEST);
        }

        // Creating new siteApplication
        User user = new User(signupForm.getName(), signupForm.getUsername(), signupForm.getEmail(),
                encoder.encode(signupForm.getPassword()));

        userRepository.save(user);

        return new ResponseEntity<String>("User created successfully!",
                HttpStatus.OK);
    }

    @ApiOperation(value = "Update a user")
    @PutMapping("/users/{id}")
    public ResponseEntity < User > updateUser(
            @ApiParam(value = "User Id to update user object", required = true) @PathVariable(value = "id") Long userId,
            @ApiParam(value = "Update user object", required = true) @Valid @RequestBody User userDetails) throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("User not found for this id :: " + userId));

        user.setEmail(userDetails.getEmail());
        user.setName(userDetails.getName());
        user.setUsername(userDetails.getUsername());

        final User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    @ApiOperation(value = "Delete a user")
    @DeleteMapping("/users/{id}")
    public Map< String, Boolean > deleteUser(
            @ApiParam(value = "User Id from which user object will be deleted", required = true) @PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));
        userRepository.delete(user);
        Map < String, Boolean > response = new HashMap< >();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

}